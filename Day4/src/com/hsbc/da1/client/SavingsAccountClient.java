package com.hsbc.da1.client;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.dao.*;
import com.hsbc.da1.*;
import java.util.*;
import util.*;
import com.hsbc.da1.service.SavingsAccountService;
import static java.lang.System.*;

public class SavingsAccountClient{
	
	public static void main(String[] args)  throws InsufficientBalanceException,CustomerNotFoundException  {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Please enter your choice: ");
		System.out.println("1.Array Backed");
		System.out.println("2.List Backed");
		System.out.println("3.Set Backed");
		System.out.println("4.JDBC Backed");
		
		System.out.println("======================================");
		
		int choice = scanner.nextInt();
		
		System.out.println(" Entered option is =  "+ choice);
		
		SavingsAccountDAO dao = SavingsAccountDAOFactory.getSavingsAccountDAO(choice);
		
		SavingsAccountService service = SavingsAccountServiceFactory.getInstance(dao);
		
		SavingsAccountController controller = new SavingsAccountController(service);

		SavingsAccount rashmi = controller.openSavingsAccount("Rashmi", (double)Math.random()*1000, "rash2398@gmail.com");
		SavingsAccount nisha = controller.openSavingsAccount("Nisha",(double)Math.random()*1000,  "nisha15151@gmail.com");
		
		
//		try{
//			controller.transfer(rashmi.getAccountNumber(), nisha.getAccountNumber(), 50_000);
//			System.out.println("The transfer from " +rashmi.getCustomerName()+ " of amount 50,000 has been made to "+nisha.getCustomerName()+". The current balance of " + rashmi.getCustomerName() + "is " + rashmi.getAccountBalance() + " and the current balance of " + nisha.getCustomerName()+ " is " + nisha.getAccountBalance());
//
//		}
//		
//		catch(InsufficientBalanceException | CustomerNotFoundException e) {
//			System.out.println(e.getMessage());
//		}
		
		System.out.println(nisha.getCustomerName());		

	}
}
