package com.hsbc.da1.model;
import java.util.*;

public class SavingsAccount implements Comparable<SavingsAccount> {
	
	private String customerName;
	private long accountNumber;
	private double accountBalance;
	//private static long counter = 1000;
	private Address address;
	private String emailAdd;
	
	
	
	public SavingsAccount(String customerName, double accountBalance, String emailAddress) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.emailAdd = emailAddress;
	}

	public SavingsAccount(String customerName, double accountBalance, Address address) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		//this.accountNumber = ++ counter;
		this.address = address;
	}
	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getAccountBalance() {
		return this.accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public long getAccountNumber() {
		return accountNumber;
	}
	public long setAccountNumber(long accountNumber) {
		return this.accountNumber = accountNumber;
	}
	
	public String getEmailAdd() {
		return emailAdd;
	}

	public void setEmailAdd(String emailAdd) {
		this.emailAdd = emailAdd;
	}

	@Override
	public int compareTo(SavingsAccount o) {
		// TODO Auto-generated method stub
		return this.getCustomerName().compareTo(o.getCustomerName());
	}
	
	
	@Override
	public String toString() {
		return "SavingsAccount [customerName=" + customerName + ", accountNumber=" + accountNumber + ", accountBalance="
				+ accountBalance + ", emailAdd=" + emailAdd + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (accountNumber ^ (accountNumber >>> 32));
		result = prime * result + ((customerName == null) ? 0 : customerName.hashCode());
		result = prime * result + ((emailAdd == null) ? 0 : emailAdd.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SavingsAccount other = (SavingsAccount) obj;
		if (accountNumber != other.accountNumber)
			return false;
		if (customerName == null) {
			if (other.customerName != null)
				return false;
		} else if (!customerName.equals(other.customerName))
			return false;
		if (emailAdd == null) {
			if (other.emailAdd != null)
				return false;
		} else if (!emailAdd.equals(other.emailAdd))
			return false;
		return true;
	}

}
