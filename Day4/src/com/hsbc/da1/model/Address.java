package com.hsbc.da1.model;

public class Address {
public String city;
public String area;
public int pincode;
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getArea() {
	return area;
}
public void setArea(String area) {
	this.area = area;
}
public int getPincode() {
	return pincode;
}
public void setPincode(int pincode) {
	this.pincode = pincode;
}


public Address(String city, String area, int pincode) {
	this.city = city;
	this.area = area;
	this.pincode = pincode;
}


}
