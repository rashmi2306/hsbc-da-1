package com.hsbc.da1.service;
import java.util.*;
import com.hsbc.da1.model.Address;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.exception.*;

public class SavingsAccountServiceImp implements SavingsAccountService {
	
	private SavingsAccountDAO dao;
	
	public SavingsAccountServiceImp(SavingsAccountDAO dao) {
		this.dao = dao;
	}

	
	@Override
	public SavingsAccount createSavingsAccount(String customerName, double accountBalance, String emailAdd) {
		// TODO Auto-generated method stub
		SavingsAccount savingsAccount = new SavingsAccount(customerName,accountBalance,emailAdd);
		SavingsAccount savingsAccountCreated = this.dao.saveSavingsAccount(savingsAccount);
		return savingsAccountCreated;
	}

//	@Override
//	public SavingsAccount createSavingsAccount(String customerName, double accountBalance, Address address) {
//		// TODO Auto-generated method stub
//		SavingsAccount savingsAccount = new SavingsAccount(customerName,accountBalance,address);
//		SavingsAccount savingsAccountCreated = this.dao.saveSavingsAccount(savingsAccount);
//		return savingsAccountCreated;
//
//	}

	@Override
	public void deleteSavingsAccount(long accountNumber){
		// TODO Auto-generated method stub
		this.dao.deleteSavingsAccount(accountNumber);
	}

	@Override
	public Collection <SavingsAccount> fetchSavingsAccounts() {
		// TODO Auto-generated method stub
		return this.dao.fetchSavingsAccounts();
	}

	@Override
	public SavingsAccount fetchAccountByPIN(int pin){
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		SavingsAccount savingsAccount = null;
		try {
			savingsAccount =  this.dao.fetchSavingsAccountByAccountId(accountNumber);
			
		}
		catch(CustomerNotFoundException e) {
			System.out.println("The account doesn't exist!");
		}
		return savingsAccount;
		
	}

	@Override
	public double withdraw(long accountId, double amount) throws InsufficientBalanceException, CustomerNotFoundException {
		// TODO Auto-generated method stub
		
		SavingsAccount savingsAccount =  this.dao.fetchSavingsAccountByAccountId(accountId);
		
		if(savingsAccount.getAccountBalance() >= amount) {
			double balance = savingsAccount.getAccountBalance() - amount;
			savingsAccount.setAccountBalance(balance);
			
			return amount;
			}
				throw new InsufficientBalanceException("You dont have sufficient balance to withdraw cash!");
		}


	@Override
	public double deposit(long accountId, double amount) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount !=null) {
			double accountBalance = savingsAccount.getAccountBalance();
			accountBalance = accountBalance + amount;
			savingsAccount.setAccountBalance(accountBalance);
			this.dao.updateSavingsAccount(accountId, savingsAccount);
			return savingsAccount.getAccountBalance();
		}
		throw new CustomerNotFoundException("The account doesn't exist");
}
		



	@Override
	public double checkBalance(long accountId) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if(savingsAccount != null) {
			return savingsAccount.getAccountBalance();
	}
		throw new CustomerNotFoundException("The account doesn't exist");
}
		


	@Override
	public double transfer(long fromAccountId, long toAccountId, double amount) throws InsufficientBalanceException, CustomerNotFoundException {
		// TODO Auto-generated method stub
		SavingsAccount fromId = this.dao.fetchSavingsAccountByAccountId(fromAccountId);
		SavingsAccount toId = this.dao.fetchSavingsAccountByAccountId(toAccountId);
		double balance = fromId.getAccountBalance();
		if(fromId !=null && toId != null & balance != 0) {
			
			this.deposit(toAccountId, amount);
			return amount;
		

	}
		return 0;
	}

	
	}

	
	



