package com.hsbc.da1.service;
import com.hsbc.da1.model.SavingsAccount;
import java.util.*;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.Address;
import com.hsbc.da1.exception.CustomerNotFoundException;

public interface SavingsAccountService {
	
	public SavingsAccount createSavingsAccount(String customerName, double accountBalance, String emailAdd);
	
	//public SavingsAccount createSavingsAccount(String customerName, double accountBalance, Address address);
	
	
	public void deleteSavingsAccount(long accountNumber);
	
	public Collection <SavingsAccount> fetchSavingsAccounts();
	
	public SavingsAccount fetchAccountByPIN(int pin) ;
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException;
	
	public double withdraw(long accountId, double amount) throws InsufficientBalanceException,CustomerNotFoundException;
	
	
	public double deposit(long accountId, double amount) throws CustomerNotFoundException;
	
	public double checkBalance(long accountId) throws CustomerNotFoundException;
	
	public double transfer(long accountId, long toId, double amount) throws InsufficientBalanceException,CustomerNotFoundException;
}
