package com.hsbc.da1.dao;
import com.hsbc.da1.model.SavingsAccount;

import java.util.*;

import com.hsbc.da1.exception.CustomerNotFoundException;

public class ArraySavingsAccountDAOImp implements SavingsAccountDAO {
	private static SavingsAccount[] savingsAccounts = new SavingsAccount[100];
	private static int counter;
	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		
		savingsAccounts[++counter] = savingsAccount;
		return savingsAccount;
	}
	
	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		for(int index=0;index<savingsAccounts.length;index++) {
			if(savingsAccounts[index].getAccountNumber() == accountNumber) {
				savingsAccounts[index] = savingsAccount;
				break;
			}
			else {
				throw new CustomerNotFoundException("Customer doesnot exist");
				
			}
		}
		return savingsAccount;
		
	}
	@Override
	public void deleteSavingsAccount(long accountNumber){
		// TODO Auto-generated method stub
		
		for(int index=0;index<savingsAccounts.length;index++) {
			if(savingsAccounts[index].getAccountNumber() == accountNumber) {
				savingsAccounts[index] = null;
			}
		}
	}
	
	@Override
	public List <SavingsAccount> fetchSavingsAccounts(){
		// TODO Auto-generated method stub
		
		return Arrays.asList(savingsAccounts);
	}
	
	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException{
		// TODO Auto-generated method stub
		
		for(int index=0;index<savingsAccounts.length;index++) {
			if(savingsAccounts[index].getAccountNumber() == accountNumber) {
				return savingsAccounts[index];
			}
		}
	throw new CustomerNotFoundException("Customer doesnot exist");
}

	@Override
	public SavingsAccount fetchSavingAccountByEmailAdd(String emailAdd) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		for(int index=0;index<savingsAccounts.length;index++) {
			if(savingsAccounts[index].getEmailAdd() == emailAdd) {
				return savingsAccounts[index];
			}
		}
		
		throw new CustomerNotFoundException("Customer doesnot exist");
	}
	
	

}
