package com.hsbc.da1.dao;
import java.util.*;

import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.exception.CustomerNotFoundException;

public interface SavingsAccountDAO {
	
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount);
	
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) throws CustomerNotFoundException;
	
	public void deleteSavingsAccount(long accountNumber);
	
	Collection <SavingsAccount> fetchSavingsAccounts();
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException;
	

	public SavingsAccount fetchSavingAccountByEmailAdd(String emailAdd) throws CustomerNotFoundException;

}
