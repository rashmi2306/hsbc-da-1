package com.hsbc.da1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public class JdbcBackedSavingsAccountDAOImp implements SavingsAccountDAO {
	
	private static String connectString = "jdbc:derby://localhost:1527/items";
	private static String username = "admin";
	private static String password = "password";
	//private static final String INSERT_QUERY = "insert into savingsaccount (cusomername, customersalary)"
			//+ " values ";
	
	private static final String INSERT_QUERY_PREP_STMT = "insert into savingsaccount (customername, customersalary, customeremailadd)" + "values (?, ?, ?)";

	private static final String SELECT_QUERY = "select * from savingsaccount";

	private static final String DELETE_BY_ID_QUERY = "delete  from savingsaccount where customerid=";
	
	private static final String SELECT_BY_ID_QUERY = "select *  from savingsaccount where customerid=";
	
	private static final String UPDATE_QUERY = "update savingsaccount set ";


	private static final String SELECT_BY_EMAIL_QUERY = "select *  from savingsaccount where email_address=";

	
	private static Statement getStatement() {
		try {
			Connection connection = DriverManager.getConnection(connectString, username, password);
			System.out.println("Successfully connected to the database ");
			return connection.createStatement();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static Connection getDBConnection() {
		try {
			Connection connection = DriverManager.getConnection(connectString, username, password);
			return connection;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}


	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		
		//String query = INSERT_QUERY+ "( " + savingsAccount.getAccountNumber() + " , '" + savingsAccount.getCustomerName() + "' , " + savingsAccount.getAccountBalance() + ");"

		int numberOfUpdatedRecords = 0;
		try (PreparedStatement pStmt =getDBConnection().prepareStatement(INSERT_QUERY_PREP_STMT); ) {
				pStmt.setString(1, savingsAccount.getCustomerName());
				pStmt.setDouble(2,savingsAccount.getAccountBalance());
				pStmt.setString(3, savingsAccount.getEmailAdd());
				numberOfUpdatedRecords = pStmt.executeUpdate();
					}
		catch(SQLException e) {
			e.printStackTrace();
		}
		
		if(numberOfUpdatedRecords ==1 ) {
			try {
				SavingsAccount fetchedSavingsAccount = fetchSavingAccountByEmailAdd(savingsAccount.getEmailAdd());
				
				if(fetchedSavingsAccount != null) {
					return savingsAccount;
				}
			}
			catch(CustomerNotFoundException e) {
				e.printStackTrace();
			}
}
		return null;
	}


	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount)
			throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		
		int numberOfUpdatedRecords = 0;
		String updateQuery = UPDATE_QUERY + "customername = '" + savingsAccount.getCustomerName() + "' , customersalary =  " + savingsAccount.getAccountBalance() + " where customerid = " + accountNumber + ";" ;
		try (PreparedStatement pStmt =getDBConnection().prepareStatement(updateQuery); ) {
			numberOfUpdatedRecords = pStmt.executeUpdate();
				}
	catch(SQLException e) {
		e.printStackTrace();
	}
		
		if(numberOfUpdatedRecords ==1 ) {
			try {
				SavingsAccount fetchedSavingsAccount = fetchSavingsAccountByAccountId(savingsAccount.getAccountNumber());
				
				if(fetchedSavingsAccount != null) {
					return savingsAccount;
				}
			}
			catch(CustomerNotFoundException e) {
				e.printStackTrace();
			}
			}
			return null;
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		// TODO Auto-generated method stub
		int numberOfDeletedRecords = 0;
		String deleteQuery = DELETE_BY_ID_QUERY + accountNumber + ";";
		try (PreparedStatement pStmt =getDBConnection().prepareStatement(deleteQuery); ) {
			numberOfDeletedRecords = pStmt.executeUpdate();
				}
		catch(SQLException e) {
		e.printStackTrace();
	}
		if(numberOfDeletedRecords ==1 )
			System.out.println("The account with account number " + accountNumber + "has been deleted successfully!");
	}

	@Override
	public Collection<SavingsAccount> fetchSavingsAccounts() {
		// TODO Auto-generated method stub
		try (PreparedStatement pStmt =getDBConnection().prepareStatement(SELECT_QUERY); ) {
			ResultSet rs = pStmt.executeQuery();
			while(rs.next()) {
				int accountNumber = rs.getInt("customerid");
				String customerName = rs.getString("customername");
				double customerSalary = rs.getDouble("customersalary");
				String customerEmailAdd = rs.getString("customeremailadd");
				System.out.println("Account Number: " + accountNumber);
				System.out.println("Customer Name: " + customerName);
				System.out.println("Customer Salary: " + customerSalary);
				System.out.println("Email Address: " + customerEmailAdd);
			}
			rs.close();
				}
	catch(SQLException e) {
		e.printStackTrace();
	}
		return null;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		String fetchQuery = SELECT_BY_ID_QUERY + accountNumber + ";";
		try (PreparedStatement pStmt =getDBConnection().prepareStatement(fetchQuery); ) {
			ResultSet rs= pStmt.executeQuery();
			if(rs!=null) {
				int accountId = rs.getInt("customerid");
				String customerName = rs.getString("customername");
				double customerSalary = rs.getDouble("customersalary");
				String customerEmailAdd = rs.getString("customeremailadd");
				System.out.println("Account Number: " + accountId);
				System.out.println("Customer Name: " + customerName);
				System.out.println("Customer Salary: " + customerSalary);
				System.out.println("Email Address: " + customerEmailAdd);
				}
			else
				throw new CustomerNotFoundException("Customer with the given account number is not found!");
		}
		catch(SQLException e) {
		e.printStackTrace();
			}
		return null;
}


	@Override
	public SavingsAccount fetchSavingAccountByEmailAdd(String emailAdd) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		String fetchQuery = SELECT_BY_EMAIL_QUERY + "'" + emailAdd + " ' ;";
		try (PreparedStatement pStmt =getDBConnection().prepareStatement(fetchQuery); ) {
			ResultSet rs= pStmt.executeQuery();
			if(rs!=null) {
				int accountId = rs.getInt("customerid");
				String customerName = rs.getString("customername");
				double customerSalary = rs.getDouble("customersalary");
				String customerEmailAdd = rs.getString("customeremailadd");
				System.out.println("Account Number: " + accountId);
				System.out.println("Customer Name: " + customerName);
				System.out.println("Customer Salary: " + customerSalary);
				System.out.println("Email Address: " + customerEmailAdd);
				}
			else
				throw new CustomerNotFoundException("Customer with the given account number is not found!");
		}
		catch(SQLException e) {
		e.printStackTrace();
			}
		return null;
	}

}
