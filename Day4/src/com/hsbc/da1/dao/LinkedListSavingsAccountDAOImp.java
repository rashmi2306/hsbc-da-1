package com.hsbc.da1.dao;

import java.util.*;
import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public class LinkedListSavingsAccountDAOImp implements SavingsAccountDAO {
	
	private List <SavingsAccount> savingsAccountLinkedList = new LinkedList<>();

	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		this.savingsAccountLinkedList.add(savingsAccount);
		return savingsAccount;
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount)
			throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		
		for(SavingsAccount sa : savingsAccountLinkedList) {
			if(sa.getAccountNumber() == accountNumber) {
				sa = savingsAccount;
			}
		}
		return savingsAccount;
	}

	@Override
	public void deleteSavingsAccount(long accountNumber){
		// TODO Auto-generated method stub
		for(SavingsAccount sa : savingsAccountLinkedList) {
			if(sa.getAccountNumber() == accountNumber) {
				this.savingsAccountLinkedList.remove(sa);
			}
		}
		
	}

	@Override
	public List<SavingsAccount> fetchSavingsAccounts() {
		// TODO Auto-generated method stub
		return this.savingsAccountLinkedList;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		
		for(SavingsAccount sa : savingsAccountLinkedList) {
			if(sa.getAccountNumber() == accountNumber) {
				return sa;
			}
		}
		return null;
	}

	@Override
	public SavingsAccount fetchSavingAccountByEmailAdd(String emailAdd) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

}
