package com.hsbc.da1.dao;
import java.util.*;
import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;


public class HashSetSavingsAccountDAOImp implements SavingsAccountDAO {

	
	private Set <SavingsAccount> savingsAccountSet = new HashSet<> ();
	
	Iterator<SavingsAccount> it = savingsAccountSet.iterator();
	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
				savingsAccountSet.add(savingsAccount);
				return savingsAccount;
	}
	

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount)
			throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		while(it.hasNext()) {
			long accountId = it.next().getAccountNumber();
			SavingsAccount sa = it.next();
			if(accountId == accountNumber) {
				sa = savingsAccount;
				return sa;
			}
		}
		
		
		return null;
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		// TODO Auto-generated method stub
		
		for(SavingsAccount sa : savingsAccountSet) {
			if(sa.getAccountNumber() == accountNumber) {
				savingsAccountSet.remove(sa);
			}
		}
	}
	

	@Override
	public Collection<SavingsAccount> fetchSavingsAccounts() {
		// TODO Auto-generated method stub
		return savingsAccountSet;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		while(it.hasNext()) {
			long accountId = it.next().getAccountNumber();
			if(accountId == accountNumber) {
				return it.next();
			}
		}
		throw new CustomerNotFoundException("Customer with the given account number doesn't exist!");
	}


	@Override
	public SavingsAccount fetchSavingAccountByEmailAdd(String emailAdd) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		while(it.hasNext()) {
			String accountEmail = it.next().getEmailAdd();
			if(accountEmail.endsWith(emailAdd)) {
				return it.next();
			}
		}
		throw new CustomerNotFoundException("Customer with the given account number doesn't exist!");
	}

}
