package com.hsbc.da1.controller;
import java.util.*;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.*;
import util.SavingsAccountServiceFactory;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.exception.CustomerNotFoundException;

public class SavingsAccountController {

	private SavingsAccountService savingsAccountService;
	
	public SavingsAccountController(SavingsAccountService savingsAccountService) {
		this.savingsAccountService = savingsAccountService;
	}
	
	
	public SavingsAccount openSavingsAccount(String customerName, double accountBalance, String emailadd) {
		SavingsAccount savingsAccount = this.savingsAccountService.createSavingsAccount(customerName, accountBalance,emailadd);	
		return savingsAccount;
	}

	public void deleteSavingsAccount(long accountNumber) throws CustomerNotFoundException {
		this.savingsAccountService.deleteSavingsAccount(accountNumber);
	}

	public Collection <SavingsAccount> fetchSavingsAccounts() {
		return this.savingsAccountService.fetchSavingsAccounts();
	}

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException {
		SavingsAccount savingsAccount = this.savingsAccountService.fetchSavingsAccountByAccountId(accountNumber);
		return savingsAccount;
	}

	public double withdraw(long accountId, double amount) throws InsufficientBalanceException, CustomerNotFoundException{
		return this.savingsAccountService.withdraw(accountId, amount);
	}
	
	public double withdrawFromATM(int pin, double amount) throws InsufficientBalanceException,CustomerNotFoundException {
		SavingsAccount savingsAccount = this.savingsAccountService.fetchAccountByPIN(pin);
		return this.savingsAccountService.withdraw(savingsAccount.getAccountNumber(), amount);
	}

	public double deposit(long accountId, double amount) throws CustomerNotFoundException {
		return this.savingsAccountService.deposit(accountId, amount);
	}

	public double checkBalance(long accountId) throws CustomerNotFoundException {
		return this.savingsAccountService.checkBalance(accountId);
	}
	
	public double transfer(long senderId, long toId, double amount) throws InsufficientBalanceException,CustomerNotFoundException{
		return this.savingsAccountService.transfer( senderId,  toId,  amount);
	}
	
	
	
	
}
