package util;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class DateAssignment {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		String dateOfBirth = scanner.next();
		System.out.println("Enter your date of birth in dd-MM-YYYY format!");
		LocalDate dob = LocalDate.parse(dateOfBirth, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		
		LocalDate currentDate = LocalDate.now();
		int numberOfDays = Period.between(dob, currentDate).getYears();
		System.out.println("Current Age is " + numberOfDays);
	}
}
