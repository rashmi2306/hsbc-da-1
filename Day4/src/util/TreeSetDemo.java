package util;
import java.util.*;
import com.hsbc.da1.model.SavingsAccount;

public class TreeSetDemo {
	public static void main(String[] args) {
		SavingsAccount rashmi = new SavingsAccount("Rashmi", 25_000);
		SavingsAccount nisha = new SavingsAccount("Nisha", 50_000);
		SavingsAccount mukesh = new SavingsAccount("Mukesh", 75_000);
		SavingsAccount jigar = new SavingsAccount("Jigar", 85_000);
		
		Set <SavingsAccount> savingsAccountSet = new TreeSet<>(new SortByAccountNameDesc());
		
		savingsAccountSet.add(rashmi);
		savingsAccountSet.add(nisha);
		savingsAccountSet.add(mukesh);
		savingsAccountSet.add(jigar);
		
		Iterator <SavingsAccount> it = savingsAccountSet.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		

	}
}

class SortByAccountNameDesc implements Comparator<SavingsAccount>{

	@Override
	public int compare(SavingsAccount o1, SavingsAccount o2) {
		// TODO Auto-generated method stub
		
		return -1 * o1.getCustomerName().compareTo(o2.getCustomerName());
	}
	
}

class SortByAccountNameAsc implements Comparator<SavingsAccount>{

	@Override
	public int compare(SavingsAccount o1, SavingsAccount o2) {
		// TODO Auto-generated method stub
		
		return o1.getCustomerName().compareTo(o2.getCustomerName());
	}
	
}

class SortByAccountNumberAsc implements Comparator<SavingsAccount>{

	@Override
	public int compare(SavingsAccount o1, SavingsAccount o2) {
		// TODO Auto-generated method stub
		
		return (int)(o1.getAccountNumber() - o2.getAccountNumber());
	}
	
}

class SortByAccountNumberDesc implements Comparator<SavingsAccount>{

	@Override
	public int compare(SavingsAccount o1, SavingsAccount o2) {
		// TODO Auto-generated method stub
		
		return -1 * (int)(o1.getAccountNumber() - o2.getAccountNumber());
	}
	
}


