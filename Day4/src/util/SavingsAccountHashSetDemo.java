package util;
import java.util.*;

import com.hsbc.da1.model.SavingsAccount;

public class SavingsAccountHashSetDemo {

	public static void main(String[] args) {
		SavingsAccount rashmi = new SavingsAccount("Rashmi", 25_000);
		rashmi.setAccountNumber(12345);
		SavingsAccount ayush = new SavingsAccount("Rashmi", 25_000);
		ayush.setAccountNumber(45678);
		
		Set <SavingsAccount> savingsAccounts = new HashSet<> ();
		
		savingsAccounts.add(rashmi);
		savingsAccounts.add(ayush);
		
		Iterator <SavingsAccount> it = savingsAccounts.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
	}
}
