package util;
import java.util.*;
import com.hsbc.da1.model.SavingsAccount;

public class HashMapDemo {
	
	public static void main(String[] args) {
		Map <String, Double> savingsAccountMap = new TreeMap<> ();
		SavingsAccount rashmi = new SavingsAccount("Rashmi", 25_000);
		SavingsAccount nisha = new SavingsAccount("Nisha", 50_000);
		SavingsAccount mukesh = new SavingsAccount("Mukesh", 75_000);
		SavingsAccount jigar = new SavingsAccount("Jigar", 85_000);
		
		savingsAccountMap.put(rashmi.getCustomerName(), rashmi.getAccountBalance());
		savingsAccountMap.put(nisha.getCustomerName(), nisha.getAccountBalance());
		savingsAccountMap.put(mukesh.getCustomerName(), mukesh.getAccountBalance());
		savingsAccountMap.put(jigar.getCustomerName(), jigar.getAccountBalance());
		
		Set <String> setOfKeys = savingsAccountMap.keySet();
		//Iterator<SavingsAccount> keys = setOfKeys.iterator();
		
		Collection <Double> savingsAccountBalance = savingsAccountMap.values(); 
		Iterator <Double> its = savingsAccountBalance.iterator();
		Set <Map.Entry<String, Double>> setofEntries = savingsAccountMap.entrySet();
		Iterator <Map.Entry<String, Double>> iter = setofEntries.iterator();
		
		while(iter.hasNext()) {
			Map.Entry<String, Double> entry = iter.next();
			System.out.printf(" Key is " +  entry.getKey() + " and value is "+ entry.getValue());
		}

		
		
	}

}
