package util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class DateDemo {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Input the date in dd--mm--yy format!");
		String inputDate = scanner.next();
		
		LocalDate dob = LocalDate.parse(inputDate, DateTimeFormatter.ofPattern("dd-mm-yy"));
		LocalDate currentDate = LocalDate.now();
		LocalDate futureDate = LocalDate.of(2021, 4, 15);
		
		currentDate.isBefore(futureDate);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yy EEEE");
		String currentDateStr = currentDate.format(formatter);
		System.out.println(currentDateStr);
		
		System.out.println("Converting from string to date :  ");
		LocalDate parsedDate = LocalDate.parse(currentDateStr, DateTimeFormatter.ofPattern("dd-MM-yy EEEE"));
		System.out.println(parsedDate);
	}

}
