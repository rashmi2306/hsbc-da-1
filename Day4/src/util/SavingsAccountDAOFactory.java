package util;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.dao.HashSetSavingsAccountDAOImp;
import com.hsbc.da1.dao.JdbcBackedSavingsAccountDAOImp;
import com.hsbc.da1.dao.LinkedListSavingsAccountDAOImp;
import com.hsbc.da1.dao.ArrayListSavingsAccountDAOImp;


public class SavingsAccountDAOFactory {
    private SavingsAccountDAOFactory() {
    }

    public static SavingsAccountDAO getSavingsAccountDAO(int value) {
        if (value == 4) {
            return new JdbcBackedSavingsAccountDAOImp();
        } else {
            return new ArrayListSavingsAccountDAOImp();
        }
    }


}
