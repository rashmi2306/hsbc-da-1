
public class TwoDimensionalMatrix {
    public static void main(String args[]){
        int rowNumber = 5;
        int colNumber = 5;
        int [][] matrix = new int[rowNumber][colNumber];

        assignMatrix(matrix);
        printMatrix(matrix);
    }


    public static void assignMatrix(int[][] matrix) {
        int initialValue = 10;
        for (int rowIndex = 0; rowIndex < 5; rowIndex++) {
            for (int colIndex = 0; colIndex < 5; colIndex++) {
                matrix[rowIndex][colIndex] = initialValue++;
            }
        }
    }

    public static void printMatrix(int[][] matrix) {
        for (int rowIndex = 0; rowIndex < 5; rowIndex++) {
            for (int colIndex = 0; colIndex < 5; colIndex++) {
                System.out.print("\t" + matrix[rowIndex][colIndex] + " \t");
            }
            System.out.println();
        }
    }

}
