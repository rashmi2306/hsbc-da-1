

public class TotalBill {
    public static void main(String args[]){
        String enterBill = args[0];
        String enterCode = args[1];
        double billDecimal = Float.valueOf(enterBill);

        String enterStateCode = enterCode.toUpperCase();

        if(enterStateCode == "KA")
            billDecimal = billDecimal *1.15;
        else if (enterStateCode == "TN")
            billDecimal = billDecimal *1.18;
        else if (enterStateCode == "MH")
            billDecimal = billDecimal *1.20;
        else
            billDecimal = billDecimal *1.12;
        System.out.println("The total bill amount is "+ billDecimal);
    }
}
