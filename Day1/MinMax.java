

public class MinMax {
    public static void main(String args[]){
        String numberOne = args[0];
        String numberTwo = args[1];
        String numberThree = args[2];
        String numberFour = args[3];
        String numberFive = args[4];

        int integerArray[] = new int[5];
        integerArray[0] = Integer.parseInt(numberOne);
        integerArray[1] = Integer.parseInt(numberTwo);
        integerArray[2] = Integer.parseInt(numberThree);
        integerArray[3] = Integer.parseInt(numberFour);
        integerArray[4] = Integer.parseInt(numberFive);
        int maxNumber = 0, minNumber = 12346;

        for(int index = 0; index<5;index++){
            if(integerArray[index] > maxNumber){
                maxNumber = integerArray[index];
            }
        }

        for(int index = 0; index<5;index++){
            if(integerArray[index] < minNumber){
                minNumber = integerArray[index];
            }
        }

        System.out.println("The maximum of the number provided is : " + maxNumber);
        System.out.println("The minimum of the number provided is : " + minNumber);

    }
}
