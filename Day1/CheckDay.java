

public class CheckDay {
    public static void main(String args[]){
        String enterDay = args[0];
        
        String capitalDay = enterDay.toLowerCase();
        if(capitalDay == "monday" || capitalDay == "tuesday" || capitalDay == "wednesday" || capitalDay == "thursday" || capitalDay == "friday")
            System.out.println("Weekday");
        else    
            System.out.println("Weekend");
    }
}
