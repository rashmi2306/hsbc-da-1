package dao;
import model.Item;


public interface ItemDAO {

	public Item fetchItemById(long itemId);
	public Item[] fetchAllItems();
	public Item updateItemByName(long itemId, String itemName);
	public Item updateItemByPrice(long itemId, double itemPrice);
	public Item saveItem(Item item);
	public void removeItem(long itemId);
}
