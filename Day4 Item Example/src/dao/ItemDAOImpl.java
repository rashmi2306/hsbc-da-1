package dao;
import model.Item;

public class ItemDAOImpl implements ItemDAO {
	
	private Item[] itemsList = new Item[1000];
	private static int itemCounter = 0;

	@Override
	public Item fetchItemById(long itemId) {
		// TODO Auto-generated method stub
		
		for(int index= 0 ;index<itemsList.length;index++) {
			if(itemsList[index].getItemId() == itemId ) {
				return itemsList[index];
			}
		}
		return null;
	}

	@Override
	public Item[] fetchAllItems() {
		// TODO Auto-generated method stub
		return itemsList;
	}

	@Override
	public Item updateItemByName(long itemId, String itemName) {
		// TODO Auto-generated method stub
		
		for(int index=0;index<itemsList.length;index++) {
			if(itemsList[index].getItemId() == itemId) {
				itemsList[index].setItemName(itemName);;
				break;
			}
		}
		return itemsList[index];
	}

	@Override
	public Item updateItemByPrice(long itemId, double itemPrice) {
		// TODO Auto-generated method stub
		
		for(int index=0;index<itemsList.length;index++) {
			if(itemsList[index].getItemId() == itemId) {
				itemsList[index].setItemPrice(itemPrice);
				break;
			}
		}
		return itemsList[index];
	}

	@Override
	public Item saveItem(Item item) {
		// TODO Auto-generated method stub
		itemsList[itemCounter++] = item;
		return itemsList[itemCounter];
	}

	@Override
	public void removeItem(long itemId) {
		// TODO Auto-generated method stub
		
		for(int index=0;index<itemsList.length;index++) {
			if(itemsList[index].getItemId() == itemId) {
				itemsList[index] = null;
			}
		}
		
	}


}
