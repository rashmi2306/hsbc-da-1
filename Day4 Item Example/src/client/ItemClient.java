package client;

import controller.ItemController;
import model.Item;

public class ItemClient {
	public static void main(String[] args) {
		ItemController itemController = new ItemController();
		
		Item bread =  itemController.saveItem("Bread", 38.00);
		Item jam = itemController.saveItem("Jam", 120.00);
		Item butter = itemController.saveItem("Butter", 50.00);
		
		Item itemsList[] = itemController.fetchAllItems();
		
		for(int index = 0;index<itemsList.length;index++) {
			if(itemsList[index].getItemId() != 0)
				System.out.println(itemsList[index].getItemName() + " " + itemsList[index].getItemId() + " " + itemsList[index].getItemPrice());
			else
				break;
		}

	}

}
