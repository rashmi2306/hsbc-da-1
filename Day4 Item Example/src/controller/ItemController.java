package controller;

import model.Item;
import service.ItemServiceImpl;
import service.ItemService;

public class ItemController {
	ItemService itemService = new ItemServiceImpl();
	
	public Item saveItem(String itemName, double itemPrice) {
		return this.itemService.createItem(itemName, itemPrice);
	}
	
	public Item[] fetchAllItems() {
		return this.fetchAllItems();
	}
	
	public Item fetchItemById(long itemId) {
		return this.itemService.fetchItemById(itemId);
	}
	
	public Item updateItem(long itemId, Item item) {
		return this.itemService.updateItem(itemId, item);
	}
	
	public void deleteItem(long itemId) {
		this.itemService.deleteItem(itemId);
	}
}
