package service;

import model.Item;
import dao.ItemDAO;
import dao.ItemDAOImpl;

public class ItemServiceImpl implements ItemService {
	
	ItemDAO dao = new ItemDAOImpl();

	@Override
	public Item createItem(String itemName, double itemPrice) {
		// TODO Auto-generated method stub
		Item item = new Item(itemName, itemPrice);
		this.dao.saveItem(item);
		return item;
	}

	@Override
	public Item[] fetchAllItems() {
		// TODO Auto-generated method stub
		return this.dao.fetchAllItems();
	}

	@Override
	public Item fetchItemById(long itemId) {
		// TODO Auto-generated method stub
		
		return this.dao.fetchItemById(itemId);
	}

	@Override
	public Item updateItemByName(long itemId, String itemName) {
		// TODO Auto-generated method stub
		
		Item itemUpdation = this.dao.updateItemByName(itemId, itemName)
		return itemUpdation;
	}

	
	@Override
	public Item updateItemByPrice(long itemId, double itemPrice) {
		// TODO Auto-generated method stub
		
		Item itemUpdation = this.dao.updateItemByPrice(itemId, itemPrice);
		return itemUpdation;
	}


	@Override
	public void deleteItem(long itemId) {
		// TODO Auto-generated method stub
		this.dao.removeItem(itemId);
		
	}

}
