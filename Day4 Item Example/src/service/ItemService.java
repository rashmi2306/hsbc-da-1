package service;

import model.Item;
public interface ItemService {
	
	Item createItem(String itemName, double itemPrice);
	Item[] fetchAllItems();
	Item fetchItemById(long itemId);
	Item updateItemByName(long itemId,String name);
	Item updateItemByPrice(long itemId, double price);

	public void deleteItem(long itemId);
}
