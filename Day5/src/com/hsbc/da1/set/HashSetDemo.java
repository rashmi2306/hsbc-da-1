package com.hsbc.da1.set;
import java.util.*;

public class HashSetDemo {

		public static void main(String[] args) {
			Set <Integer> set = new HashSet<>();
			
			set.add(24);
			set.add(25);
			set.add(26);
			set.add(27);
			set.add(25);
			set.add(27);
			
			System.out.println("Total number of elements in the set is " + set.size());
			
			Iterator <Integer> it = set.iterator();
			while(it.hasNext()) {
				System.out.println(it.next());
			}
		}
}
