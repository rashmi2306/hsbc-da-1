package com.hsbc.da1.exceptions;

public class ExceptionDemo {
	
	public static void main(String[] args) {
		int numberOne = 2;
		int numberTwo = 0;
		int integerArray[] = {1,2,3,4};
		
		try {
			//System.out.println("divison " + numberOne/numberTwo);
			System.out.println("last element of array " + integerArray[integerArray.length]);
		}
		
		catch(ArithmeticException  | ArrayIndexOutOfBoundsException e ) {
			System.out.println("Arithmetic type " + (e instanceof ArithmeticException));
			System.out.println("ArrayIndexOutOfBounds type " + (e instanceof ArrayIndexOutOfBoundsException));
		}
		
		catch(Exception e) {
			System.out.println("Generic Exception");
		}
		
		finally {
			System.out.println("Gets executed whether exception is handled or not");
		}
	}

	
	
}
