package com.hsbc.employeemgmnt.model;
import java.util.*;

public class EmployeeManagement implements Comparable<EmployeeManagement> {

	private String employeeName;
	private long employeeId;
	private int employeeAge;
	private double employeeSalary;
	private int employeeLeaves;
	private static int counter = 1000;
	private static final int totalLeaves = 40;
	
	public EmployeeManagement (String employeeName, int employeeAge, double employeeSalary) {
		this.employeeName = employeeName;
		this.employeeAge = employeeAge;
		this.employeeId = ++counter;
		this.employeeLeaves = totalLeaves;
		this.employeeSalary = employeeSalary;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public int getEmployeeAge() {
		return employeeAge;
	}

	public void setEmployeeAge(int employeeAge) {
		this.employeeAge = employeeAge;
	}

	public double getEmployeeSalary() {
		return employeeSalary;
	}

	public void setEmployeeSalary(double employeeSalary) {
		this.employeeSalary = employeeSalary;
	}

	public int getEmployeeLeaves() {
		return employeeLeaves;
	}

	public void setEmployeeLeaves(int employeeLeaves) {
		this.employeeLeaves = employeeLeaves;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + employeeAge;
		result = prime * result + (int) (employeeId ^ (employeeId >>> 32));
		result = prime * result + employeeLeaves;
		result = prime * result + ((employeeName == null) ? 0 : employeeName.hashCode());
		long temp;
		temp = Double.doubleToLongBits(employeeSalary);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeManagement other = (EmployeeManagement) obj;
		if (employeeAge != other.employeeAge)
			return false;
		if (employeeId != other.employeeId)
			return false;
		if (employeeLeaves != other.employeeLeaves)
			return false;
		if (employeeName == null) {
			if (other.employeeName != null)
				return false;
		} else if (!employeeName.equals(other.employeeName))
			return false;
		if (Double.doubleToLongBits(employeeSalary) != Double.doubleToLongBits(other.employeeSalary))
			return false;
		return true;
	}

	@Override
	public int compareTo(EmployeeManagement o) {
		// TODO Auto-generated method stub
		return (int)(this.getEmployeeId() - o.getEmployeeId());
	}


	
	
}
