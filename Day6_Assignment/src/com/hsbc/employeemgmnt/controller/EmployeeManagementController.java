package com.hsbc.employeemgmnt.controller;
import java.util.*;
import com.hsbc.employeemgmnt.exception.NotSufficientLeavesException;
import com.hsbc.employeemgmnt.model.*;
import com.hsbc.employeemgmnt.service.*;

public class EmployeeManagementController {
	
	private EmployeeManagementService employee;
	
	public EmployeeManagement createEmployee(String employeeName, int employeeAge, int salary) {
		EmployeeManagement newEmployee = this.employee.createNewEmployee(employeeName, employeeAge, salary);
		return newEmployee;
	}
	
	public void deleteEmployee(long employeeId) {
		this.employee.deleteEmployee(employeeId);
	}
	
	public EmployeeManagement fetchEmployeeInfoById(long employeeId) {
		EmployeeManagement fetchEmployee = this.employee.fetchEmployeeInfoById(employeeId);
		return fetchEmployee;

	}
	public EmployeeManagement fetchEmployeeInfoByName(String employeeName) {
		EmployeeManagement fetchEmployee = this.employee.fetchEmployeeInfoByName(employeeName);
		return fetchEmployee;

	}
	Collection <EmployeeManagement> fetchEmployeeInfo(){
		return this.employee.fetchEmployeeInfo();
	}
	public void applyForLeave(long employeeId, int leaves) throws NotSufficientLeavesException {
		this.employee.applyForLeave(employeeId, leaves);
	}
	public int getNumberOfLeaves(long employeeId) {
		return this.employee.getNumberOfLeaves(employeeId);
	}


}
