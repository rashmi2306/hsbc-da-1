package com.hsbc.employeemgmnt.dao;
import java.util.*;
import com.hsbc.employeemgmnt.model.EmployeeManagement;

public class EmployeeManagementDAOImpl implements EmployeeManagementDAO {
	
	private Map <Long, EmployeeManagement> employeeInfoMap = new TreeMap<>();

	@Override
	public EmployeeManagement saveEmployeeInfo(long employeeId, EmployeeManagement employeeInfo) {
		// TODO Auto-generated method stub
		employeeInfoMap.put(employeeId, employeeInfo);
		return employeeInfo;
	}

	@Override
	public EmployeeManagement updateEmployeeInfo(long employeeId, EmployeeManagement employeeNewInfo) {
		// TODO Auto-generated method stub
		Set <Long> setOfKeys = employeeInfoMap.keySet();
		Iterator <Long> keys = setOfKeys.iterator();
		while(keys.hasNext()) {
			long key = keys.next();
			if(key == employeeId) {
				employeeInfoMap.replace(employeeId, employeeNewInfo);
			}
		}
		return employeeNewInfo;
	}

	@Override
	public EmployeeManagement fetchEmployeeInfoById(long employeeId) {
		// TODO Auto-generated method stub
		
		
		Set <Map.Entry<Long, EmployeeManagement>> setofEntries = employeeInfoMap.entrySet();
		Iterator <Map.Entry<Long, EmployeeManagement>> iter = setofEntries.iterator();
		
		while(iter.hasNext()) {
			Map.Entry<Long, EmployeeManagement> entry = iter.next();
			if(entry.getKey() == employeeId)
			System.out.println("EmployeeId : " + entry.getKey() + "EmployeeName: " + entry.getValue().getEmployeeName() + " EmployeeAge: " + entry.getValue().getEmployeeAge() + " EmployeeSalary : " + entry.getValue().getEmployeeSalary() + " EmployeeLeavesLeft : " + entry.getValue().getEmployeeLeaves());
			
		}
		return null;
	}

	@Override
	public Collection <EmployeeManagement> fetchEmployeeInfo() {
		// TODO Auto-generated method stub
		return employeeInfoMap.values();
	}

	@Override
	public void deleteEmployeeById(long employeeId) {
		// TODO Auto-generated method stub
		Set <Long> setOfKeys = employeeInfoMap.keySet();
		Iterator <Long> keys = setOfKeys.iterator();
		while(keys.hasNext()) {
			long key = keys.next();
			if(key == employeeId) {
				employeeInfoMap.remove(key); 
			}
		}
		
	}

	@Override
	public EmployeeManagement fetchEmployeeInfoByName(String employeeName) {
		// TODO Auto-generated method stub
		Set <Map.Entry<Long, EmployeeManagement>> setofEntries = employeeInfoMap.entrySet();
		Iterator <Map.Entry<Long, EmployeeManagement>> iter = setofEntries.iterator();
		
		while(iter.hasNext()) {
			Map.Entry<Long, EmployeeManagement> entry = iter.next();
			if(entry.getValue().getEmployeeName() == employeeName)
			System.out.println("EmployeeId : " + entry.getKey() + "EmployeeName: " + entry.getValue().getEmployeeName() + " EmployeeAge: " + entry.getValue().getEmployeeAge() + " EmployeeSalary : " + entry.getValue().getEmployeeSalary() + " EmployeeLeavesLeft : " + entry.getValue().getEmployeeLeaves());
			
		}
		return null;
	}

}
