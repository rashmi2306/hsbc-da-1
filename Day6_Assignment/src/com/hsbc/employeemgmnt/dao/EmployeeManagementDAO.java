package com.hsbc.employeemgmnt.dao;
import java.util.*;

import com.hsbc.employeemgmnt.model.*;

public interface EmployeeManagementDAO {
	public EmployeeManagement saveEmployeeInfo(long employeeId, EmployeeManagement employeeInfo);
	public EmployeeManagement updateEmployeeInfo(long employeeId, EmployeeManagement employeeNewInfo);
	public EmployeeManagement fetchEmployeeInfoById(long employeeId);
	public EmployeeManagement fetchEmployeeInfoByName(String employeeName);
	Collection <EmployeeManagement> fetchEmployeeInfo();
	public void deleteEmployeeById(long employeeId);
}
