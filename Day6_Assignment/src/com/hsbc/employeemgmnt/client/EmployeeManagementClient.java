package com.hsbc.employeemgmnt.client;
import com.hsbc.employeemgmnt.controller.EmployeeManagementController;
import com.hsbc.employeemgmnt.exception.NotSufficientLeavesException;
import com.hsbc.employeemgmnt.model.EmployeeManagement;

public class EmployeeManagementClient {
	public static void main(String[] args) {
		EmployeeManagementController controller = new EmployeeManagementController();
		
		controller.createEmployee("Rashmi", 22, 50_000);
		controller.createEmployee("Jigar", 23, 60_000);
		
		EmployeeManagement employee = controller.fetchEmployeeInfoById(1001);
		System.out.println(employee.getEmployeeLeaves());
		
		try {
			controller.applyForLeave(employee.getEmployeeId(), 8);
		}
		catch(NotSufficientLeavesException e) {
			System.out.println(e.getMessage());
		}
		
	}

}
