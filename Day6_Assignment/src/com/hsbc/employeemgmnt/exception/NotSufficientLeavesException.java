package com.hsbc.employeemgmnt.exception;

public class NotSufficientLeavesException extends Exception{
	public NotSufficientLeavesException(String message) {
		super(message);
	}
	
	@Override
	public String getMessage() {
		return super.getMessage();
	}
	
	
}
