package com.hsbc.employeemgmnt.service;
import java.util.*;

import com.hsbc.employeemgmnt.dao.*;
import com.hsbc.employeemgmnt.exception.*;
import com.hsbc.employeemgmnt.model.EmployeeManagement;

public class EmployeeManagementServiceImpl implements EmployeeManagementService {
	
	private EmployeeManagementDAO dao;
	
	public EmployeeManagementServiceImpl (EmployeeManagementDAO dao) {
		this.dao = dao;
	}

	@Override
	public EmployeeManagement createNewEmployee(String employeeName, int employeeAge, double salary) {
		// TODO Auto-generated method stub
		
		EmployeeManagement newEmployee = new EmployeeManagement(employeeName, employeeAge, salary);
		EmployeeManagement newEmployeeCreated = this.dao.saveEmployeeInfo(newEmployee.getEmployeeId(), newEmployee);
		return newEmployeeCreated;
	}


	@Override
	public void deleteEmployee(long employeeId) {
		// TODO Auto-generated method stub
		this.dao.deleteEmployeeById(employeeId);
		
	}

	@Override
	public EmployeeManagement fetchEmployeeInfoById(long employeeId) {
		// TODO Auto-generated method stub
		EmployeeManagement employeeInfo = null;
		employeeInfo = this.dao.fetchEmployeeInfoById(employeeId);
		return employeeInfo;
	}

	@Override
	public EmployeeManagement fetchEmployeeInfoByName(String employeeName) {
		// TODO Auto-generated method stub
		EmployeeManagement employeeInfo = null;
		employeeInfo = this.dao.fetchEmployeeInfoByName(employeeName);
		return employeeInfo;
	}

	@Override
	public Collection<EmployeeManagement> fetchEmployeeInfo() {
		// TODO Auto-generated method stub
		return this.dao.fetchEmployeeInfo();
	}

	@Override
	public void applyForLeave(long employeeId, int leaves) throws NotSufficientLeavesException {
		// TODO Auto-generated method stub
		EmployeeManagement employeeInfo = this.dao.fetchEmployeeInfoById(employeeId);
		int initialLeaves = employeeInfo.getEmployeeLeaves();
		if(leaves > 10) {
			System.out.println("Continuous leave for more than 10 days cannot be approved");
		}
		else {
			if(initialLeaves > leaves) {
				initialLeaves = initialLeaves - leaves;
				employeeInfo.setEmployeeLeaves(initialLeaves);
				System.out.println("The request for leaves has been applied!");
			}
			else {
				throw new NotSufficientLeavesException("Not sufficient leaves present to take a leave!");
			}

		}
		
		
	}

	@Override
	public int getNumberOfLeaves(long employeeId) {
		// TODO Auto-generated method stub
		EmployeeManagement employeeInfo = this.dao.fetchEmployeeInfoById(employeeId);
		return employeeInfo.getEmployeeLeaves();
	
	}

}
