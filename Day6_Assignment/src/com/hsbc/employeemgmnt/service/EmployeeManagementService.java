package com.hsbc.employeemgmnt.service;
import java.util.*;

import com.hsbc.employeemgmnt.exception.*;
import com.hsbc.employeemgmnt.model.*;

public interface EmployeeManagementService {
	
	public EmployeeManagement createNewEmployee(String employeeName, int employeeAge, double Salary);
	public void deleteEmployee(long employeeId);
	public EmployeeManagement fetchEmployeeInfoById(long employeeId);
	public EmployeeManagement fetchEmployeeInfoByName(String employeeName);
	Collection <EmployeeManagement> fetchEmployeeInfo();
	public void applyForLeave(long employeeId, int leaves) throws NotSufficientLeavesException;
	public int getNumberOfLeaves(long employeeId);

}
