public class CallByValRef {
    public static void main(String args[]){
        int integerArray[] = new int[]{1,2,3,4};
        int numberOne = 33;
        int numberTwo = 39;
    
        //call by reference
       
         System.out.println("Array before the call : ");
        for(int index:integerArray){
            System.out.println(index);
        }
        System.out.println("---------------");
        callByRef(integerArray);
        System.out.println("---------------");
        System.out.println("Array after callbyref ");
        for(int index:integerArray){
            System.out.println(index);
        }

        //call by value

        System.out.println("Numbers before the call");
        System.out.println(" "+ numberOne + " " + numberTwo);
        System.out.println("---------------------");
        callByVal(numberOne, numberTwo);
        System.out.println("---------------------");
        System.out.println("Numbers after the call");
        System.out.println(" "+ numberOne + " " + numberTwo);
}

    
    //method for call by reference
    private static void callByRef(int[] integerArray){
        for(int index=0;index<4;index++){
            integerArray[index] = integerArray[index]*index;
        
        }
        for(int number:integerArray){
            System.out.println(number);
        }
    }

    //method for call by value
    private static void callByVal(int numberOne, int numberTwo){
        numberOne = numberOne + 22;
        numberTwo = numberTwo + 44;

        System.out.println("the numbers inside the method are " + numberOne + " " + numberTwo);
    }


}
