public class CurrentAccountRegister {
    private static CurrentAccount[] currentAccounts = new CurrentAccount[] {
        new CurrentAccount("Rashmi", "shop","AS1234567",50_000, new Address("Pune", "Woodsville", 123456,"Maharashtra")),
        new CurrentAccount("Vinay", "shop", "WQ1234455",90_000,new Address("Bangalore", "Phase1", 897068, "Karnataka"))

};

public static CurrentAccount fetchSavingsAccountByAccountId(long accountId) {

    for (CurrentAccount savingsAccount : currentAccounts) {
        if (savingsAccount.getAccountNumber() == accountId) {
            return savingsAccount;
        }
    }
    return null;
}

}
