

public class CurrentAccount {
    private long accountNumber;
    private double accountBalance;
    private String gstNumber;
    private String customerName;
    private String businessName;
    private Address address;
    

    static private long minBalance = 50_000;
    private static long accountNumberTracker = 10_000;

    public long getAccountNumber(){
        return accountNumber;
    }

    public CurrentAccount(String customerName, String businessName, String gstNumber,double accountBalance){
        this.customerName = customerName;
        this.businessName = businessName;
        this.gstNumber = gstNumber;
        this.accountNumber = ++accountNumberTracker;
        this.accountBalance = accountBalance;
    }

    public CurrentAccount(String customerName,String businessName, String gstNumber,double accountBalance, Address address){
        this.customerName = customerName;
        this.accountNumber = ++accountNumberTracker;
        this.accountBalance = accountBalance;
        this.address = address;
    }
 
    

    public boolean withdrawMoney(double withdrawAmount){
        if(withdrawAmount <= (this.accountBalance-minBalance)){
            this.accountBalance = this.accountBalance - withdrawAmount;
            System.out.println("The remaining balance is "+ this.accountBalance + " after thr withdrawal of " + withdrawAmount);
            return true;
        }
        else{
            System.out.println("Not enough balance present for withdrawal of the amount!!");
            return false;
        }
}

    public void deposit(double depositAmount) {
        this.accountBalance = this.accountBalance + depositAmount;
        System.out.println("The balance after deposition is " + this.accountBalance);
    }

    public void transferAmount(double amount,CurrentAccount user){
        
        if((this.accountBalance-50000) > amount){
            boolean flag = this.withdrawMoney(amount);
            if(flag){
                user.deposit(amount);
                System.out.println("The transfer has been done successfully!!");
            }
            
        }
    }
        
    public double checkBalance() {
        return this.accountBalance;
    }

    
    public boolean validateAmount(double amount){
        if(amount >50_000){
            return true;
        }
        return false;
    }
    

    public void updateAddress(Address address) {
        this.address = address;
    }

    public String getCustomerName() {
        return this.customerName;
    }


}
