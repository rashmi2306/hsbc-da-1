package com.hsbc.da1.thread;

public class RunnableDemo {

	public static void main(String[] args) {
		Runnable domesticDelivery = new DeliveryIntraState();
		Runnable nationalDelivery = new DeliveryInterState();
		Runnable internationalDelivery = new DeliveryInternational();
		
		Thread domestic = new Thread (domesticDelivery);
		domestic.setName("Domestic Delivery");
		domestic.setPriority(1);
		
		Thread national = new Thread (nationalDelivery);
		national.setName("National Delivery");
		national.setPriority(9);
		
		Thread international = new Thread (internationalDelivery);
		international.setName("International Delivery");
		international.setPriority(6);
		
		domestic.start();
		national.start();
		international.start();
		
		try {
			domestic.join();
			national.join();
			international.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("All Items are delivered");
		
	}

}

class DeliveryInterState implements Runnable {

	@Override
	public void run() {
		System.out.println("Thread "+ Thread.currentThread()+ " has started!!");
		
		for(int i = 0 ;i < 10; i ++) {
			System.out.println("Thread running "  + i + " name: " + Thread.currentThread());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Thread "+ Thread.currentThread()+ " has ended");
	}
}

class DeliveryIntraState implements Runnable {

	@Override
	public void run() {
		System.out.println("Thread "+ Thread.currentThread()+ " has started!!");
		
		for(int i = 0 ;i < 10; i ++) {
			System.out.println("Thread running "  + i + " name: " + Thread.currentThread());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Thread "+ Thread.currentThread()+ " has ended");
	}
}

class DeliveryInternational implements Runnable {

	@Override
	public void run() {
		System.out.println("Thread "+ Thread.currentThread()+ " has started!!");
		
		for(int i = 0 ;i < 10; i ++) {
			System.out.println("Thread running "  + i + " name: " + Thread.currentThread());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Thread "+ Thread.currentThread()+ " has ended");
	}

	}

