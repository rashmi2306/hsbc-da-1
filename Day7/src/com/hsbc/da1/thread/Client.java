package com.hsbc.da1.thread;

public class Client {
	public static void main(String[] args) {
		
		Runnable googlePhotos = new GooglePhotos();
		Runnable flickr = new Flickr();
		Runnable picassa = new Picassa();
		
		Thread googleThread = new Thread(googlePhotos);
		googleThread.setName("GooglePhotos");
		googleThread.setPriority(4);
		
		Thread flickrThread = new Thread(flickr);
		flickrThread.setName("Flickr");
		flickrThread.setPriority(7);
		
		Thread picassaThread = new Thread(picassa);
		picassaThread.setName("Picassa");
		picassaThread.setPriority(2);
		
		googleThread.start();
		flickrThread.start();
		picassaThread.start();
		
		
		try {
			googleThread.join();
			flickrThread.join();
			picassaThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Photos from all sources downloaded!!");
		
		
	}

}

class GooglePhotos implements Runnable{

	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("Current Thread : " + Thread.currentThread().getName());
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

class Flickr implements Runnable{

	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("Current Thread : " + Thread.currentThread().getName());
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

class Picassa implements Runnable{

	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("Current Thread : " + Thread.currentThread().getName());
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

