interface PaymentGateway{
    void pay(String from, String to, double amountPay, String notes );
}

interface MobileRecharge{
    void rechargeMobile(String mobileNumber, double amountPay);
}

class GooglePay implements PaymentGateway,MobileRecharge{
    public void pay(String from, String to, double amountPay, String notes){
        System.out.println("The amount " + amountPay + " has been done to " + to + " from " + from +" " + notes +  "via Gpay");

    }

    public void rechargeMobile(String mobileNumber, double amountPay){
        System.out.println("The mobile number  " + mobileNumber + " has been recharged with an amount of Rs. "+amountPay);

    }
}


class PhonePay implements PaymentGateway, MobileRecharge{
    public void pay(String from, String to, double amountPay, String notes){
        System.out.println("The amount " + amountPay + " has been done to " + to + " from " + from +" " + notes +  "via PhonePay");

    }

    public void rechargeMobile(String mobileNumber, double amountPay){
        System.out.println("The mobile number  " + mobileNumber + " has been recharged with an amount of Rs. "+amountPay);

    }
}

class JioPay implements PaymentGateway{
    public void pay(String from, String to, double amountPay, String notes){
        System.out.println("The amount " + amountPay + " has been done to " + to + " from " + from +" " + notes +  "via JioPay");

    }
}


public class PaymentGatewayClient {
    public static void main(String[] args) {
        PaymentGateway paymentGateway = null;
        MobileRecharge mobileRecharge = null;

        if(args[0] == "1"){
            GooglePay gpay = new GooglePay();
            paymentGateway = gpay;
            mobileRecharge = gpay;
        }
        else if (args[0] == "2"){
            PhonePay ppay = new PhonePay();
            paymentGateway = ppay;
            mobileRecharge = ppay;
        }
        else{
            JioPay jpay = new JioPay();
            paymentGateway = jpay;
        }

        paymentGateway.pay("Rashmi", "Nisha", 5000, "Done");
        
        if(mobileRecharge !=null){
            mobileRecharge.rechargeMobile("7410010086", 555);
        }
        
    }
}
