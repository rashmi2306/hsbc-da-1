abstract class BankAccount{

    public abstract boolean withdraw();
    public abstract boolean minBalance();
    public abstract double checkBalance();
    public abstract void transferAmount();
    public abstract void LoanApproval();
    public final void deposit(double amount){
        this.initialBalance +=amount;
    }

    public String name;
    private double initialBalance;
    private final long accountNumber;
    private static long accountNumberTracker = 1000;
    Address address;
   


    public BankAccount(String name, double initialBalance, Address address){
        this.name = name;
        this.initialBalance = initialBalance;
        this.address = address;
        accountNumber = ++accountNumberTracker;
    }

    

}
    
  public class BankAccountClient {
        public static void main (String[] args) {
            //Salaried Account
            Address addressSalaried = new Address("Chennai", "XYZ", 123587, "Tamil Nadu");
            
            //CurrentAccount
            Address addressCurrent = new Address("Pune" , "Jadhavwadi" , 456787, "Maharashtra");
            
            //SavingAccount
            Address addressSaving = new Address("Delhi", "ABC", 123567, "Delhi");

            //BankAccount bankAccountOne = null;
            //BankAccount bankAccountTwo = null;
            String typeofAccount = args[0];
            if(typeofAccount == "1"){
                 CurrentAccount bankAccountOne = new CurrentAccount("Rashmi", "shop" , "AS12345564", 23_47_383, addressCurrent);
                 CurrentAccount bankAccountTwo = new CurrentAccount("Nisha","shop","WE12345567",51000,addressCurrent);
                bankAccountOne.transferAmount(1234564, bankAccountTwo);
            }

            else if(typeofAccount == "2"){
                SavingAccount bankAccountOne = new SavingAccount("Ayush", 123456745, addressSaving);
                SavingAccount bankAccountTwo = new SavingAccount("Rahul", 87654323, addressSaving);
                bankAccountOne.transferAmount(213456, bankAccountTwo);
            }

            else if(typeofAccount == "3"){
                SalariedAccount bankAccountOne = new SalariedAccount("Stuti", 1234545, addressSalaried);
                SalariedAccount bankAccountTwo = new SalariedAccount("Saurabh", 6654322, addressSalaried);
                bankAccountOne.transferAmount(23456,bankAccountTwo);
            }
    
    
    
        }
    }

    //salaried account
    final class SalariedAccount extends BankAccount {
        private static long accountNumberTracker = 1000;
        final private long accountNumber;
        private double accountBalance;
        private String customerName;
        private Address address; 
    
        final public long getAccountNumber() {
            return accountNumber;
        }
        public SalariedAccount(String customerName, double initialAccountBalance, Address address) {
            super(customerName, initialAccountBalance,address);
            this.accountNumber = ++accountNumberTracker;
        }
    
        // instance methods
        final public boolean withdraw(double amount) {
            if(amount <= 15_000){
            if (this.accountBalance >= amount) {
                this.accountBalance = this.accountBalance - amount;
                return true;
            }
        }
            else{
                System.out.println("You cannot withdraw more than Rs. 15,000!!");
            }
            return false;
        }
    
        final public void transferAmount(double amount,SalariedAccount user){
            
            if((this.accountBalance-50000) > amount){
                boolean flag = this.withdraw(amount);
                if(flag){
                    user.deposit(amount);
                    System.out.println("The transfer has been done successfully!!");
                }
                
            }
        }
    
        final public void LoanApproval(double amount){
            if(amount <= 10_00_000){
                System.out.println("Loan approved!!");
            }
            else
                System.out.println("Loan amount exceeds Rs. 10lakhs!");
        }
    
        final public double checkBalance() {
            return this.accountBalance;
        }
    
    
        final public void updateAddress(Address address) {
            this.address = address;
        }
    
        final public String getCustomerName() {
            return this.customerName;
    
        }
    
        final public boolean minBalance(double amount){
            if(amount >0){
                System.out.println("The minimum balance must be 0!!");
                return true;
            }
            return true;
        }
    }
    
    
    

    //current account
    final class CurrentAccount extends BankAccount{
        final private long accountNumber;
        private double accountBalance;
        private String gstNumber;
        private String customerName;
        private String businessName;
        private Address address;
        
    
        static private long minBalance = 50_000;
        private static long accountNumberTracker = 10_000;
    
        final  public long getAccountNumber(){
            return accountNumber;
        }
    
    
        public CurrentAccount(String customerName,String businessName, String gstNumber,double accountBalance, Address address){
           super(customerName,accountBalance,address);
           this.businessName = businessName;
           this.gstNumber = gstNumber;
           this.accountNumber = ++accountNumberTracker;
        }
        final public boolean withdraw(double withdrawAmount){
            if(withdrawAmount <= (this.accountBalance-minBalance)){
                this.accountBalance = this.accountBalance - withdrawAmount;
                System.out.println("The remaining balance is "+ this.accountBalance + " after thr withdrawal of " + withdrawAmount);
                return true;
            }
                return false;
    }
    
    final public void transferAmount(double amount,CurrentAccount user){
            
        if((this.accountBalance-50000) > amount){
            boolean flag = this.withdraw(amount);
            if(flag){
                user.deposit(amount);
                System.out.println("The transfer has been done successfully!!");
            }
            
        }
    }
    
    final public void LoanApproval(double amount){
        if(amount <= 25_00_000){
            System.out.println("Loan approved!!");
        }
        else
            System.out.println("Loan amount exceeds Rs, 25lakhs!");
    }
    
    
    final public double checkBalance() {
            return this.accountBalance;
        }
    
        final public void updateAddress(Address address) {
            this.address = address;
        }
    
        final public String getCustomerName() {
            return this.customerName;
        }
    
        final public void minBalance(double amount){
            if(amount <=25_000){
                System.out.println("The minimum balance must be 25,000!!");
            }
        }
    }
    
    final class SavingAccount extends BankAccount{

        private static long accountNumberTracker = 1000;
        final private long accountNumber;
        private double accountBalance;
        private String customerName;
        private Address address; 
    
        public long getAccountNumber() {
            return accountNumber;
        }
    
    
        public SavingAccount(String customerName, double initialAccountBalance, Address address) {
            super(customerName,initialAccountBalance,address);
            this.accountNumber = ++accountNumberTracker;
        }
    
        // instance methods
        final public boolean withdraw(double amount) {
            if(amount<=10_000){
                if (this.accountBalance >= amount) {
                    this.accountBalance = this.accountBalance - amount;
                    return true;
                }
            }
            else{
                System.out.println("You cannot withdraw more than Rs. 10,000!!");
            }
            
            return false;
        }
    
    
        final public void transferAmount(double amount,SavingAccount user){
            
            if((this.accountBalance-50000) > amount){
                boolean flag = this.withdraw(amount);
                if(flag){
                    user.deposit(amount);
                    System.out.println("The transfer has been done successfully!!");
                }
                
            }
        }
        final public void LoanApproval(double amount){
            if(amount <= 5_00_000){
                System.out.println("Loan approved!!");
            }
            else
                System.out.println("Loan amount exceeds Rs, 5lakhs!");
        }
    
    
    
        final public double checkBalance() {
            return this.accountBalance;
        }
    
    
        final public void updateAddress(Address address) {
            this.address = address;
        }
    
        final public String getCustomerName() {
            return this.customerName;
    
        }
    
        final public void minBalance(double amount){
            if(amount <=10000){
                System.out.println("The minimum balance must be 10000!!");
            }
        }
    
    }
    
     class Address {
        private String city;
        private String street;
        private int zipcode;
        private String state;
    
    
        public Address (String city,String street, int zipcode, String state){
            this.city = city;
            this.street = street;
            this.zipcode = zipcode;
            this.state = state;
        }
    
        public String getCity() {
            return city;
        }
    
        public void setCity(String city) {
            this.city = city;
        }
    
        public String getStreet() {
            return street;
        }
    
        public void setStreet(String street) {
            this.street = street;
        }
    
        public int getZipcode() {
            return zipcode;
        }
    
        public void setZipcode(int zipcode) {
            this.zipcode = zipcode;
        }
    
        public String getState() {
            return state;
        }
    
        public void setState(String state) {
            this.state = state;
        }
    
        
    }
                
 
